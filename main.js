/*var vid = document.getElementById('vid');
var vidLayout = document.getElementById('vidLayout');
var vidPlay = document.getElementById('vidPlay');
var vidPause = document.getElementById('vidPause');
var vidControls = document.getElementById('vidControls');
var vidPlayer = document.getElementById('vidPlayer');
var vidIcons = document.getElementById('vidIcons');
var vidDuration = document.getElementById('vidDuration');
var vidCurrentDuration = document.getElementById('vidCurrentDuration');
var vidVolumeUp = document.getElementById('vidVolumeUp');
var vidVolumeDown = document.getElementById('vidVolumeDown');

var seekslider = document.getElementById('seekslider');

vidPlay.addEventListener('click', function(){
    vid.play();
    vidLayout.style.display = "none";
    vidIcons.innerHTML = '<i class="fas fa-pause" id="vidPause"></i>';
})


vidIcons.addEventListener('click', function(){
    if(vid.paused) {
        vid.play();
        vidIcons.innerHTML = '<i class="fas fa-pause" id="vidPause"></i>';
    } else {
        vid.pause();
        vidIcons.innerHTML = '<i class="fas fa-play" id="vidPlayer"></i>';
    }
});


seekslider.addEventListener('change', function(){
    var seekto = vid.duration * (seekslider.value / 100);
    vid.currentTime = seekto;
})

vid.addEventListener('timeupdate', function() {
    var newTime = vid.currentTime * (100 / vid.duration);
    seekslider.value = newTime;
    vidCurrentDuration.innerText = vid.currentTime;
});



vid.addEventListener('canplay', function(){
    vidDuration.innerText = vid.duration;
})
// vid.addEventListener('timeupdate', function(){
//     vidCurrentDuration.innerText = vid.currentTime;
// })

vidVolumeDown.addEventListener('click', function(){
    if(vid.volume - 0.1 < 0) {
        vid.volume = 0;
        currentVolumPosition = vid.volume;
        vidVolumeDown.innerHTML = '<i class="fas fa-volume-mute" id="vidVolumeMute"></i>'
        return;
    }
    vid.volume = vid.volume - 0.1;
    currentVolumPosition = vid.volume;
});

vidVolumeUp.addEventListener('click', function() {
    if(vid.volume + 0.1 > 1) {
        vid.volume = 1;
        currentVolumPosition = vid.volume;
        return;
    }
    vid.volume = vid.volume + 0.1;
    if(vid.volume = vid.volume + 0.1) {
        vidVolumeDown.innerHTML = '<i class="fas fa-volume-down"></i>'
    };
    currentVolumPosition = vid.volume;
})
vid.onended = function() {
    vidIcons.innerHTML = '<i class="fas fa-play" id="vidPlayer"></i>';
    vidLayout.style.display = "block";
};*/


var vid = document.getElementById('myVideo');
var playVideo = document.getElementById('playVideo');
var pauseVideo = document.getElementById('pauseVideo');
var volumeVideo = document.getElementById('volumeVideo');
var muteVideo = document.getElementById('muteVideo');
var volumeSeek = document.getElementById('volumeSeek');
var vidDuration = document.getElementById('vidDuration');
var vidCurrentDuration = document.getElementById('vidCurrentDuration');

var fullscreen = document.getElementById('fullscreen');
var slideSeek = document.getElementById('slideSeek');


vid.addEventListener('canplay', function() {
    pauseVideo.style.display = "none";
    muteVideo.style.display = "none";
    vidDuration.innerText = (timeConvert(vid.duration));
})


playVideo.addEventListener('click', function() {
    vid.play();
    playVideo.style.display = "none";
    pauseVideo.style.display = "";
})

pauseVideo.addEventListener('click', function() {
    vid.pause();
    playVideo.style.display = "";
    pauseVideo.style.display = "none";
})

volumeVideo.addEventListener('click', function() {
    vid.volume = 0;
    volumeVideo.style.display = "none";
    muteVideo.style.display = "";
    volumeSeek.value = 0;
})

muteVideo.addEventListener('click', function() {
    vid.volume = 1;
    volumeVideo.style.display = "";
    muteVideo.style.display = "none";
    volumeSeek.value = 100;
})

// volumeSeek.addEventListener('change', function(){
//     var seektoVolume = vid.volume * (volumeSeek.value / 100);
//     vid.volume = seektoVolume;
// })

volumeSeek.addEventListener('change', function(){
   var seektoVolume = vid.volume * (volumeSeek.value / 100);
        if(vid.volume - 0.1 < 0) {
            vid.volume = 0;
            currentVolumPosition = vid.volume;
            volumeVideo.style.display = "none";
            muteVideo.style.display = "";
            return;
        }
        vid.volume = seektoVolume;
        currentVolumPosition = vid.volume;

      
         
    
        
});
// volumeSeek.addEventListener('change', function(){
//     var seektoVolumed = vid.volume * (volumeSeek.value / 100);
//     if(vid.volume + 0.1 > 1) {
//         vid.volume = 1;
//         currentVolumPosition = vid.volume;
//         return;
//     }
//     vid.volume = vid.volume + 0.1;
//     vid.volume = seektoVolumed;
    
//     if(vid.volume = vid.volume + 0.1) {
//         // volumeVideo.style.display = "";
//         // muteVideo.style.display = "none";
//     };
//     currentVolumPosition = vid.volume;
    
    
        
// });




fullscreen.addEventListener('click', function(){
    vid.requestFullscreen();
})


slideSeek.addEventListener('change', function(){
    var seektoVideo = vid.duration * (slideSeek.value / 100);
    vid.currentTime = seektoVideo;
    playVideo.style.display = "none";
    pauseVideo.style.display = "";
})

vid.addEventListener('timeupdate', function() {
    var newTime = vid.currentTime * (100 / vid.duration);
    slideSeek.value = newTime;
    vidCurrentDuration.innerText = (timeConvert(vid.currentTime));
   
    // vidCurrentDuration.innerText = vid.currentTime;
});
slideSeek.addEventListener('click', function() {
    playVideo.style.display = "none";
    pauseVideo.style.display = "";
})



function timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return  rhours + "." + rminutes ;
    }